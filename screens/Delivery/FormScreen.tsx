import Axios from 'axios'
import {
  Body,
  Button,
  Card,
  CardItem,
  Container,
  Content,
  Fab,
  Form,
  Header,
  Icon,
  Input,
  Item,
  Label,
  Right,
  Text,
  Textarea,
  Title,
  View,
} from 'native-base'
import React from 'react'
import { useDispatch, useSelector } from 'react-redux'
import CustomCheckBox from '../../components/CustomCheckBox'
import CustomInput from '../../components/CustomInput'
import CustomPicker from '../../components/CustomPicker'
import { update } from '../../slices/deliverySlice'
import { DeliveryObject } from '../../typings'

export default function ({ navigation }: any) {
  const form = useSelector((state: any) => state.delivery.form)
  const dispatch = useDispatch()

  function onFieldChanged(field: string, value: any) {
    let data = { ...form, objects: form.objects || [], [field]: value }

    const quantity = Number(value)

    if (field === 'quantity' && quantity !== data.objects.length) {
      while (data.objects.length !== quantity) {
        if (data.objects.length < quantity) {
          data.objects = [
            ...data.objects,
            {
              id: Math.random().toString(16),
              text: `Object #${data.objects.length + 1}`,
            },
          ]
        } else if (data.objects.length > quantity) {
          data.objects.pop()

          data.objects = [...data.objects]
        }
      }
    }

    dispatch(update(data))
  }

  function onObjectFieldChange(field: string, value: any, index: number) {
    const data = [...form.objects]

    data[index] = { ...data[index], [field]: value }

    console.log('onObjectFieldChange data[index]', data[index])

    dispatch(update({ ...form, objects: data }))
  }

  function submit() {
    Axios.post('https://dry-peak-64713.herokuapp.com/deliveries', form, {
      headers: { 'X-Requested-With': 'XMLHttpRequest' },
    })
      .then(console.log)
      .catch(console.error)
  }

  return (
    <Container>
      <Header>
        <Body>
          <Title>Delivery</Title>
        </Body>

        <Right>
          <Button hasText transparent onPress={() => navigation.navigate('DeliveryList')}>
            <Text>List</Text>
          </Button>
        </Right>
      </Header>

      <Content padder>
        <Form>
          <CustomInput
            onChangeText={(v) => onFieldChanged('support_address', v)}
            placeholder="Support Address"
          />

          <CustomInput
            onChangeText={(v) => onFieldChanged('deposit_address', v)}
            placeholder="Deposit Address"
          />

          <CustomInput
            onChangeText={(v) => onFieldChanged('recipient', v)}
            placeholder="Recipient's name"
          />

          <CustomPicker
            onValueChange={(v) => onFieldChanged('supported', v)}
            selectedValue={form.supported}
            items={[
              {
                id: Math.random().toString(16),
                text: 'as soon as possible',
                value: 'as soon as possible',
              },
              {
                id: Math.random().toString(16),
                text: 'this morning',
                value: 'this morning',
              },
              {
                id: Math.random().toString(16),
                text: 'this afternoon',
                value: 'this afternoon',
              },
              {
                id: Math.random().toString(16),
                text: 'this evening',
                value: 'this evening',
              },
              {
                id: Math.random().toString(16),
                text: 'tomorrow morning',
                value: 'tomorrow morning',
              },
              {
                id: Math.random().toString(16),
                text: 'tomorrow afternoon',
                value: 'tomorrow afternoon',
              },
              {
                id: Math.random().toString(16),
                text: 'tomorrow evening',
                value: 'tomorrow evening',
              },
              {
                id: Math.random().toString(16),
                text: 'in two days, morning',
                value: 'in two days, morning',
              },
              {
                id: Math.random().toString(16),
                text: 'in two days, afternoon',
                value: 'in two days, afternoon',
              },
              {
                id: Math.random().toString(16),
                text: 'in two days, evening',
                value: 'in two days, evening',
              },
            ]}
          />

          <CustomCheckBox
            checked={form.express}
            onPress={() => onFieldChanged('express', !form.express)}
            text="Express"
          />

          <CustomInput
            keyboardType="numeric"
            onChangeText={(v) => onFieldChanged('quantity', Number(v))}
            placeholder="Quantity"
          />

          <CustomCheckBox
            checked={form.insurance}
            onPress={() => onFieldChanged('insurance', !form.insurance)}
            text="Insurance"
          />

          <Text style={{ fontSize: 24.0, marginBottom: 16 }}>
            Objects to delivered
          </Text>

          {(form.objects || []).map((obj: DeliveryObject, index: number) => (
            <Card key={obj.id} style={{ marginBottom: 16 }}>
              <CardItem header>
                <Text>{obj.text}</Text>
              </CardItem>

              <CardItem>
                <Body>
                  <CustomPicker
                    onValueChange={(v) => onObjectFieldChange('type', v, index)}
                    selectedValue={obj.type}
                    items={[
                      { id: 'bag', text: 'Bag', value: { id: 1, text: 'Bag' } },
                    ]}
                  />

                  <Text style={{ marginBottom: 16.0 }}>Size:</Text>

                  <View style={{ display: 'flex', flexDirection: 'row' }}>
                    <Item
                      style={{ flex: 1, marginLeft: 0.0, marginBottom: 16.0 }}
                    >
                      <Label>L:</Label>

                      <Input
                        keyboardType="numbers-and-punctuation"
                        onChangeText={(v) =>
                          onObjectFieldChange('length', v, index)
                        }
                        placeholder="cm"
                      />
                    </Item>

                    <View style={{ width: 16 }}></View>

                    <Item
                      style={{ flex: 1, marginLeft: 0.0, marginBottom: 16.0 }}
                    >
                      <Label>H:</Label>

                      <Input
                        keyboardType="numbers-and-punctuation"
                        onChangeText={(v) =>
                          onObjectFieldChange('height', v, index)
                        }
                        placeholder="cm"
                      />
                    </Item>
                  </View>

                  <View style={{ display: 'flex', flexDirection: 'row' }}>
                    <Item
                      style={{ flex: 1, marginLeft: 0.0, marginBottom: 16.0 }}
                    >
                      <Label>l:</Label>

                      <Input
                        keyboardType="numbers-and-punctuation"
                        onChangeText={(v) =>
                          onObjectFieldChange('width', v, index)
                        }
                        placeholder="cm"
                      />
                    </Item>

                    <View style={{ width: 16 }}></View>

                    <Item
                      style={{ flex: 1, marginLeft: 0.0, marginBottom: 16.0 }}
                    >
                      <Label>Weight:</Label>

                      <Input
                        keyboardType="numbers-and-punctuation"
                        onChangeText={(v) =>
                          onObjectFieldChange('weight', v, index)
                        }
                        placeholder="g"
                      />
                    </Item>
                  </View>

                  <Textarea
                    rowSpan={5}
                    underline={false}
                    bordered
                    placeholder="Comments"
                    onChangeText={(v) =>
                      onObjectFieldChange('comments', v, index)
                    }
                    style={{ width: '100%' }}
                  />
                </Body>
              </CardItem>
            </Card>
          ))}
        </Form>

        <Button full onPress={submit}>
          <Text>Submit</Text>
        </Button>
      </Content>
    </Container>
  )
}
