import { NavigationContainer } from '@react-navigation/native'
import { createStackNavigator } from '@react-navigation/stack'
import React from 'react'

import DeliveryFormScreen from './screens/Delivery/FormScreen'
import DeliveryListScreen from './screens/Delivery/ListScreen'

export default function Navigation() {
  return (
    <NavigationContainer>
      <RootNavigator />
    </NavigationContainer>
  )
}

const Stack = createStackNavigator()

function RootNavigator() {
  return (
    <Stack.Navigator initialRouteName="DeliveryForm">
      <Stack.Screen
        name="DeliveryForm"
        component={DeliveryFormScreen}
        options={{ headerShown: false }}
      />

      <Stack.Screen
        name="DeliveryList"
        component={DeliveryListScreen}
        options={{ headerShown: false }}
      />
    </Stack.Navigator>
  )
}
