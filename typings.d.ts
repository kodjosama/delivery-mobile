export interface DeliveryObjectType {
  id?: string
  text?: string
}

export interface DeliveryObject {
  id?: string
  text?: string
  type?: DeliveryObjectType
  length?: number
  width?: number
  height?: number
  weight?: number
  comments?: string
}

export interface Delivery {
  id?: string,
  support_address?: string
  deposit_address?: string
  recipient?: string
  supported?: string
  express?: boolean
  quantity?: number
  insurance?: boolean
  objects?: Array<DeliveryObject>
}
