import { Item, Picker } from 'native-base'
import React from 'react'

export default function (props: {
  onValueChange?: (itemValue: any, itemPosition: number) => void
  selectedValue?: any
  items: Array<{ id: string; text: string; value: any }>
}) {
  return (
    <Item regular style={{ marginLeft: 0, marginBottom: 16 }}>
      <Picker
        onValueChange={props.onValueChange}
        selectedValue={props.selectedValue}
      >
        <Picker.Item label="" value={null} />
        {props.items.map((item) => (
          <Picker.Item key={item.id} label={item.text} value={item.value} />
        ))}
      </Picker>
    </Item>
  )
}
