import { createSlice } from '@reduxjs/toolkit'
import Axios from 'axios'
import { Delivery } from '../typings'

export const slice = createSlice({
  name: 'delivery',
  initialState: {
    data: [],
    form: {},
    loading: false,
  },
  reducers: {
    setLoading: (state, { payload }) => {
      state.loading = payload
    },
    setData: (state, { payload }) => {
      state.data = payload
    },
    update: (state, { payload }) => {
      state.form = { ...state.form, ...payload }
    },
  },
})

export const { setLoading, setData, update } = slice.actions

export default slice.reducer

export function fetchDeliveries() {
  return async (dispatch: any) => {
    dispatch(setLoading(true))

    Axios.get('https://dry-peak-64713.herokuapp.com/deliveries')
      .then(({ data }) => {
        console.log('axios', data)
        
        dispatch(setData(data))
      })
      .catch(() => dispatch(setData([])))
      .finally(() => dispatch(setLoading(false)))
  }
}
