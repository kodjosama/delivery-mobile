import { configureStore } from '@reduxjs/toolkit'

import deliveryReducer from './slices/deliverySlice'

export default configureStore({
  reducer: {
    delivery: deliveryReducer,
  },
})
