import { ListItem, CheckBox, Body, Text } from 'native-base'
import React from 'react'

export default function (props: {
  checked: boolean
  onPress?: () => void
  text: string
}) {
  return (
    <ListItem noBorder style={{ marginLeft: 0, marginBottom: 16 }}>
      <CheckBox checked={props.checked} onPress={props.onPress} />

      <Body>
        <Text>{props.text}</Text>
      </Body>
    </ListItem>
  )
}
