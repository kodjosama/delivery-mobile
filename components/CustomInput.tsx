import { Input, Item } from 'native-base'
import React from 'react'
import { KeyboardTypeOptions } from 'react-native'

export default function (props: {
  keyboardType?: KeyboardTypeOptions
  onChangeText?: (text: string) => void
  placeholder: string
}) {
  return (
    <Item regular style={{ marginLeft: 0, marginBottom: 16 }}>
      <Input
        keyboardType={props.keyboardType}
        onChangeText={props.onChangeText}
        placeholder={props.placeholder}
      />
    </Item>
  )
}
