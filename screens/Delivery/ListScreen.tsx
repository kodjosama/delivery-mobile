import {
  Container,
  Header,
  Body,
  Title,
  Content,
  Text,
  Card,
  CardItem,
  View,
  Left,
  Button,
  Icon,
  List,
  ListItem,
} from 'native-base'
import React, { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { fetchDeliveries } from '../../slices/deliverySlice'
import { Delivery, DeliveryObject } from '../../typings'

export default function ({ navigation }: any) {
  const dispatch = useDispatch()
  const { loading, data } = useSelector((state: any) => state.delivery)

  useEffect(() => {
    dispatch(fetchDeliveries())
  }, [dispatch])

  if (loading) {
    return (
      <Container>
        <Header>
          <Left>
            <Button transparent onPress={() => navigation.goBack()}>
              <Icon name="arrow-back" />
            </Button>
          </Left>

          <Body>
            <Title>Deliveries</Title>
          </Body>
        </Header>

        <Content
          padder
          contentContainerStyle={{
            flex: 1,
            alignItems: 'center',
            justifyContent: 'center',
          }}
        >
          <Text>Loading...</Text>
        </Content>
      </Container>
    )
  }

  return (
    <Container>
      <Header>
        <Body>
          <Title>Deliveries</Title>
        </Body>
      </Header>

      <Content padder>
        {data.map((delivery: Delivery) => (
          <Card key={delivery.id}>
            <CardItem>
              <Body>
                <CustomText
                  text="Support Address"
                  value={delivery.support_address}
                />

                <CustomText
                  text="Deposit Address"
                  value={delivery.deposit_address}
                />

                <CustomText
                  text="Recipient's Name"
                  value={delivery.recipient}
                />

                <CustomText text="Supported" value={delivery.supported} />

                <CustomText text="Express" value={delivery.express} />

                <CustomText text="Quantity" value={delivery.quantity} />

                <Text style={{ marginBottom: 16 }}>Objects</Text>

                {delivery.objects?.map((object: DeliveryObject) => (
                  <>
                    <CustomText text="Type" value={object.type?.text} />
                    <CustomText text="Length" value={object.length} />
                    <CustomText text="Width" value={object.width} />
                    <CustomText text="Height" value={object.height} />
                    <CustomText text="Weight" value={object.weight} />
                    <CustomText text="Comments" value={object.comments} />
                  </>
                ))}
              </Body>
            </CardItem>
          </Card>
        ))}
      </Content>
    </Container>
  )
}

const CustomText = function (props: { text: string; value: any }) {
  return (
    <View style={{ display: 'flex', flexDirection: 'row', marginBottom: 16 }}>
      <Text>{props.text}:</Text>
      <View style={{ width: 8 }} />
      <Text style={{ fontSize: 12 }}>{props.value}</Text>
    </View>
  )
}
